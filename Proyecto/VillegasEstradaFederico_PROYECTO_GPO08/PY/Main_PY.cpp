#include <iostream>
#include <cmath>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "SOIL2/SOIL2.h"

void KeyCallback( GLFWwindow *window, int key, int scancode, int action, int mode );
void MouseCallback( GLFWwindow *window, double xPos, double yPos );
void DoMovement( );
void animacion();

const GLuint WIDTH = 900, HEIGHT = 650;
int SCREEN_WIDTH, SCREEN_HEIGHT;

Camera  camera( glm::vec3( 30.0f, 8.0f, 30.0f ) );

GLfloat lastX = WIDTH / 2.0;
GLfloat lastY = HEIGHT / 2.0;
bool keys[1024];
bool firstMouse = true;

GLfloat deltaTime = 0.0f;	
GLfloat lastFrame = 0.0f;  

float rotKitPuerta = 0.0;
bool abrirPuerta = false;
bool cerrarPuerta = false;

glm::vec3 PosIniCajonFrente(11.5, 4.25, -53.0);
bool abrirCajon = false;
bool cerrarCajon = false;
float movKitZCajon = 0.0;

glm::vec3 PosIniCohete(0.250f, 8.30f, -24.0f);
bool subirCohete = false;
bool bajarCohete = false;
bool rotacionCohete = false;
bool rotacionCohete2 = false;
float movKitYCohete = 0.0;
float rotKitCohete = 0.0;

int main() {
    glfwInit( );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
    glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE );
    glfwWindowHint( GLFW_RESIZABLE, GL_FALSE );
    GLFWwindow* window = glfwCreateWindow( WIDTH, HEIGHT, "Proyecto", nullptr, nullptr );

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
    if ( nullptr == window ) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate( );
        return EXIT_FAILURE;
    }
    
    glfwMakeContextCurrent( window );
    glfwGetFramebufferSize( window, &SCREEN_WIDTH, &SCREEN_HEIGHT );

    glfwSetKeyCallback( window, KeyCallback );
    glfwSetCursorPosCallback( window, MouseCallback );
    
    glewExperimental = GL_TRUE;
    
    if ( GLEW_OK != glewInit( ) )
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return EXIT_FAILURE;
    }
    
    glViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
    
    glEnable( GL_DEPTH_TEST );

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    Shader shaderModel("Shdrs/modelLoading.vs", "Shdrs/modelLoading.frag");
	Shader shader("Shdrs/core.vs", "Shdrs/core.frag");

    Model timmyModel((char*)"Models/Timmy/timmy.obj");
    Model cosmoModel((char*)"Models/Cosmo/cosmo.obj");
    Model wandaModel((char*)"Models/Wanda/wanda.obj");

    Model coheteModel((char*)"Models/Cohete/cohete.obj");
    Model coheteLateralModel((char*)"Models/Cohete/laterales.obj");
    Model coheteVentanasModel((char*)"Models/Cohete/ventanas.obj");

    Model estrellaModel((char*)"Models/Estrella/estrella.obj");

    Model brazoLamparaModel((char*)"Models/Lampara/brazoLampara.obj");
    Model focoLamparaModel((char*)"Models/Lampara/foco.obj");

    Model antenaExtremosModel((char*)"Models/Antena/antenaExtremos.obj");
    Model antenaPlatoModel((char*)"Models/Antena/antenaPlato.obj");

    Model caminoModel((char*)"Models/Camino/camino.obj");

    Model pez1((char*)"Models/cosmoPez/cosmo.obj");
    Model pez2((char*)"Models/wandaPez/wanda.obj");

    
    GLfloat vertices[] = {
        //casa
        //---

        //frente
        0, 0, 0,  
        5, 0, 0, 
        5, 5, 0, 
        0, 5, 0, //3

        //cortina
        0, 5, 0,
        2.5, 5, 0,
        1.25, 2.35, 0,
        0, 5, 0,
        0, 0, 0,
        2.4, 0, 0, 
        //---
        2.5, 5, 0,
        5, 5, 0, 
        4, 1.8, 0,
        2.7, 0, 0,
        5, 0, 0, 
        5, 5, 0, //15

        //cubo

        -2.5f,  -2.5f,  2.5f, //16
         2.5f,  -2.5f,  2.5f,
         2.5f,   2.5f,  2.5f,
        -2.5f,   2.5f,  2.5f,
        -2.5f,  -2.5f, -2.5f,
         2.5f,  -2.5f, -2.5f,
         2.5f,   2.5f, -2.5f,
        -2.5f,   2.5f, -2.5f,
        2.5f,   -2.5f,  2.5f,
        2.5f,   -2.5f, -2.5f,
        2.5f,    2.5f, -2.5f,
        2.5f,    2.5f,  2.5f,
        -2.5f,  2.5f,  2.5f,
        -2.5f,  2.5f, -2.5f,
        -2.5f, -2.5f, -2.5f,
        -2.5f, -2.5f,  2.5f,
        -2.5f, -2.5f, -2.5f,
         2.5f, -2.5f, -2.5f,
         2.5f, -2.5f,  2.5f,
        -2.5f, -2.5f,  2.5f,
        - 2.5f, 2.5f, -2.5f,
         2.5f, 2.5f, -2.5f,
         2.5f, 2.5f,  2.5f,
        -2.5f, 2.5f,  2.5f, //39

         -0.5, -0.5, 0.15,
          0.5, -0.5, 0.15,
          0.5, -0.5, -0.15,
         -0.5, -0.5, -0.15,
       //---
         -0.3, 0.5, 0.15,
          0.3,  0.5, 0.15,
          0.3,  0.5, -0.15,
         -0.3, 0.5, -0.15, //47

         //lampara
         0,       0    , 0,  //48
         0.020,   0.03 , 0,
         0.05,    0.08,  0,
         0.1,     0.1  , 0,
         0.15,    0.08,  0,
         0.18,    0.03,  0,
         0.2,     0,     0,
         0.18,   -0.03,  0,
         0.15,   -0.08,  0,
         0.1,    -0.1 ,  0,
         0.05,   -0.08,  0,
         0.02,   -0.03,  0,

        -0.1,      0,    -0.5, //60
        -0.08,   0.13,   -0.5,
        -0.05,   0.18,   -0.5,
         0.1,    0.2,    -0.5,
         0.25,   0.18,   -0.5,
         0.28,   0.13,   -0.5,
         0.3,    0,      -0.5,
         0.28,  -0.13,   -0.5,
         0.25,  -0.18,   -0.5,
         0.1,   -0.2,    -0.5,
        -0.05,  -0.18,   -0.5,
        -0.08,  -0.13,   -0.5, //71
        //base lampara
         -0.3, -0.3,  0.05, //72
          0.3, -0.3,  0.05,
          0.3, -0.3, -0.05,
         -0.3, -0.3, -0.05,
    
         -0.1, 0.15,  0.05,
          0.1, 0.15,  0.05,
          0.1, 0.15, -0.05,
         -0.1, 0.15, -0.05, //79

         0, 0, 0, //80
         1, 0, 0, 
         1, 1, 0, 
         0, 1, 0, 

         0, 0, -1,
         1, 0, -1, //85

             
    };

    unsigned int indices[] = {
        0, 1, 3,    //Cuadrado (2 triangulos)
        1, 2, 3,

        0, 1, 2, 3, //Cuadrado (contorno)

        0, 1, //linea

        4, 6, 5,
        7, 8, 9,
        10, 12, 11,
        13, 14, 15,  //cortinas {24}

        16, 17, 19, //cubo
        17, 18, 19,
        20, 21, 23,
        21, 22, 23,
        24, 25, 27,
        25, 26, 27,
        28, 29, 31,
        29, 30, 31,
        32, 33, 35,
        33, 34, 35,
        36, 37, 39,
        37, 38, 39,  //{60}

        0, 1, 5, //Triangulo 

        //chimenea (piramide)
        40, 41, 43, //66
        41, 42, 43,
        44, 45, 47,
        45, 46, 47,

        40, 45, 44, //78
        40, 41, 45, 
        43, 46, 47, 
        43, 42, 46, 
        40, 44, 47, 
        43, 40, 47, 
        41, 45, 46, 
        42, 41, 46,  //99

        //lampara
        48, 60, 49, //102
        60, 49, 61, 
        49, 61, 50, 
        61, 50, 62, 
        50, 62, 51, 
        62, 51, 63, 
        51, 63, 52, 
        63, 52, 64, 
        52, 64, 53, 
        64, 53, 65, 
        53, 65, 54, 
        65, 54, 66, 
        54, 66, 55, 
        66, 55, 67, 
        55, 67, 56, 
        67, 56, 68, 
        56, 68, 57, 
        68, 57, 69, 
        57, 69, 58, 
        69, 58, 70, 
        58, 70, 59, 
        70, 59, 71, 
        59, 71, 48, 
        71, 48, 60,  //171
        //base lampara

        72, 73, 75, 
        73, 74, 75, 
        76, 77, 79,
        77, 78, 79, 

        72, 77, 76, 
        72, 73, 77, 
        75, 78, 79, 
        75, 74, 78,

        72, 76, 79, 
        75, 72, 79, //201

        73, 77, 78,
        74, 73, 78,

        80, 81, 83, //207
        81, 82, 83,

        82, 83, 85,
        83, 84, 85,
        83, 84, 80,
        82, 85, 81,
        80, 81, 84,
        81, 85, 84,

    };
    
    GLuint VBO, VAO, EBO;
    glGenVertexArrays( 1, &VAO );
    glGenBuffers( 1, &VBO );
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);
    
    glBindBuffer( GL_ARRAY_BUFFER, VBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_STATIC_DRAW );

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
   
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( GLfloat ), ( GLvoid * )0 );
    glEnableVertexAttribArray(0);
   
    /*glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof( GLfloat ), ( GLvoid * )( 3 * sizeof( GLfloat ) ) );
    glEnableVertexAttribArray( 1 );*/

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    
    glm::mat4 projection = glm::perspective( camera.GetZoom( ), ( GLfloat )SCREEN_WIDTH / ( GLfloat )SCREEN_HEIGHT, 0.1f, 200.0f );

    glm::vec3 colorPared = glm::vec3(0.99f, 0.96f, 0.83f);
    glm::vec3 colorParedCochera = glm::vec3(0.709, 0.690, 0.525);
    glm::vec3 colorParedTrasera = glm::vec3(0.721, 0.560, 0.333);
    glm::vec3 colorTechoCochera = glm::vec3(0.733, 0.278, 0.270);
    glm::vec3 colorTechoCocheraDentro = glm::vec3(0.8, 0.6, 0.321);
    glm::vec3 colorVentana = glm::vec3(0.890, 0.886, 0.870);
    glm::vec3 colorCortina = glm::vec3(0.541, 0.584, 0.466);
    glm::vec3 colorPuerta = glm::vec3(0.764, 0.631, 0.627);
    glm::vec3 colorLineaNegra = glm::vec3(0.0, 0.0, 0.0);
    glm::vec3 colorEscalon = glm::vec3(0.862, 0.870, 0.819);
    glm::vec3 colorEscalon2 = glm::vec3(0.737, 0.737, 0.647);
    glm::vec3 colorTecho = glm::vec3(0.835, 0.435, 0.352);
    glm::vec3 colorChimeneaLateral = glm::vec3(0.392, 0.160, 0.164);
    glm::vec3 colorChimeneaLateralArriba = glm::vec3(0.572, 0.682, 0.796);
    glm::vec3 colorChimeneaFrente = glm::vec3(0.674, 0.274, 0.168);
    glm::vec3 colorChimeneaFrenteArriba = glm::vec3(0.784, 0.870, 0.925);
    glm::vec3 colorTechoCasa = glm::vec3(0.160, 0.262, 0.392);
    glm::vec3 colorParedHabitacion = glm::vec3(0.380, 0.658, 0.839);
    glm::vec3 colorSueloHabitacion = glm::vec3(0.662, 0.870, 0.972);
    glm::vec3 colorPuertaHabitacion = glm::vec3(0.549, 0.749, 0.870); 
    glm::vec3 colorPuertaLateralHabitacion = glm::vec3(0.466, 0.529, 0.572);
    glm::vec3 colorLineasSueloHabitacion = glm::vec3(0.439, 0.709, 0.776);
    glm::vec3 colorMuebleFrente = glm::vec3(0.603, 0.737, 0.866); 
    glm::vec3 colorMuebleLateral = glm::vec3(0.372, 0.474, 0.592);
    glm::vec3 colorLampara = glm::vec3(0.588, 0.721, 0.815);
    glm::vec3 colorLamparaBase = glm::vec3(0.466, 0.580, 0.690);
    glm::vec3 colorLibreroFrente = glm::vec3(0.615, 0.737, 0.831);
    glm::vec3 colorLibreroLateral = glm::vec3(0.301, 0.372, 0.423);
    glm::vec3 colorLibro = glm::vec3(0.529, 0.647, 0.678);
    glm::vec3 colorLineaLibro = glm::vec3(0.392, 0.501, 0.533); 
    glm::vec3 colorLineaCajonEscritorio = glm::vec3(0.435, 0.576, 0.698); 
    glm::vec3 colorLineasLampara = glm::vec3(0.388, 0.498, 0.572); 
    glm::vec3 colorComputadoraFrente = glm::vec3(0.631, 0.698, 0.749); 
    glm::vec3 colorComputadoraLateral = glm::vec3(0.466, 0.529, 0.572); 
    glm::vec3 colorPantallaComputadora = glm::vec3(0.439, 0.501, 0.627);
    glm::vec3 colorMarcoCuadro = glm::vec3(0.603, 0.741, 0.901); 
    glm::vec3 colorPasto = glm::vec3(0.635, 0.588, 0.388); 
    glm::vec3 colorCamino = glm::vec3(0.866, 0.874, 0.827);

    while ( !glfwWindowShouldClose( window ) ) {

        GLfloat currentFrame = glfwGetTime( );
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        
        glfwPollEvents( );
        DoMovement( );
        animacion();
        
        glClearColor(0.980, 0.815, 0.513, 1.0);
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        
        shader.Use( );
        //shaderModel.Use();

        glm::mat4 model = glm::mat4(1.0f);
        
        GLint viewPosLoc = glGetUniformLocation(shader.Program, "viewPos");
        glUniform3f( viewPosLoc, camera.GetPosition( ).x, camera.GetPosition( ).y, camera.GetPosition( ).z );
        
        glm::mat4 view;
        view = camera.GetViewMatrix( );

        GLint modelLoc = glGetUniformLocation(shader.Program, "model" );
        GLint viewLoc = glGetUniformLocation(shader.Program, "view" );
        GLint projLoc = glGetUniformLocation(shader.Program, "projection" );
        GLint uniformColor = shader.uniformColor;
        
        glUniformMatrix4fv( viewLoc, 1, GL_FALSE, glm::value_ptr( view ) );
        glUniformMatrix4fv( projLoc, 1, GL_FALSE, glm::value_ptr( projection ) );
        glUniformMatrix4fv( modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        
        
        glBindVertexArray( VAO );


       

        //--- Frente casa

        //----------
        //cubo 39, 24
        //triangulo 3, 60
        
        // izq
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorPared));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.5f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(2.5f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.5f, 5.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.75f, 5.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.50f, 10.0f, 0.0f));
        model = glm::scale(model, glm::vec3(6.0f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //dentro
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.250f, -0.1f, 0.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(12.0f, 2.750f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(14.7f, -0.1f, 0.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(12.0f, 2.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //pared trasera
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.25f, 0.0f, -60.0f));
        model = glm::scale(model, glm::vec3(5.2f, 2.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //ventana lateral trasera
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorVentana));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.3f, 5.0f, -25.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.3f, 5.0f, -40.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //cochera
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorParedCochera));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-18.75f, 4.25f, -7.0f));
        model = glm::scale(model, glm::vec3(2.5f, 1.75f, 3.85f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorPared));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-18.75f, 4.25f, -7.0f));
        model = glm::scale(model, glm::vec3(2.5f, 1.75f, 3.85f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-18.75f, 4.25f, -7.0f));
        model = glm::scale(model, glm::vec3(2.5f, 1.75f, 3.85f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));

        //cochera techo TRIANGULO

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-25.0125f, 8.6f, 2.63f));
        model = glm::scale(model, glm::vec3(2.5f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, (GLvoid*)(60 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-25.0125f, 8.6f, -16.6f));
        model = glm::scale(model, glm::vec3(2.5f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, (GLvoid*)(60 * sizeof(int)));

        //----------
        
        //

        //der
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(5.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(2.5f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(5.0f, 5.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(13.75f, 5.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //frente casa arriba

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.50f, 16.750f, -3.0f));
        model = glm::scale(model, glm::vec3(6.0f, 0.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.50f, 24.25f, -3.0f));
        model = glm::scale(model, glm::vec3(6.0f, 0.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.50f, 19.250f, -3.0f));
        model = glm::scale(model, glm::vec3(0.90f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.25f, 19.250f, -3.0f));
        model = glm::scale(model, glm::vec3(0.85f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(3.75f, 19.250f, -3.0f));
        model = glm::scale(model, glm::vec3(0.85f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.75f, 19.250f, -3.0f));
        model = glm::scale(model, glm::vec3(1.15f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //pared 2 piso
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(2.50f, 20.250, -13.05));
        model = glm::scale(model, glm::vec3(6.0f, 2.5f, 4.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(1.7, 13.75, -31.0));
        model = glm::scale(model, glm::vec3(5.20f, 0.15f, 11.75));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        //----------

        //pared trasera
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorParedTrasera));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(17.25f, 6.75, -13.15));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(3.95f, 2.75f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));


        //puerta
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorPuerta));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 1.25f, 0.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //puerta trasera
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.5f, 0.05f, -60.1f));
        model = glm::scale(model, glm::vec3(1.0f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //puerta cochera

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-23.0f, -0.2f, 2.64f));
        model = glm::scale(model, glm::vec3(1.750f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //ventana puerta

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorVentana));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.01f, 6.0f, -1.5f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.375f, 0.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //adentro
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorVentana));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-0.01f, 6.0f, -1.5f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.375f, 0.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //----------

        //ventana der
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorVentana));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 5.0f, 0.0f));   
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //dentro
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 5.0f, -0.01f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //ventana izq
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.75f, 5.0f, 0.0f)); 
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //dentro
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorVentana));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 5.0f, -0.01f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //ventanas arriba 
        //der
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.0f, 19.250f, -3.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //centro
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 19.250f, -3.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //izq
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.0f, 19.250f, -3.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //ventana trasera 
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.76f, 4.15, -10.28));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //ventana lateral 2 piso
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(17.60f, 20, -10.28));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //cortinas
        //abajo
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorCortina));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.75f, 5.0f, 0.01f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(12 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 5.0f, 0.01f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(12 * sizeof(int)));

        //dentro
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.75f, 5.0f, -0.02f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(12 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 5.0f, -0.02f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(12 * sizeof(int)));
        
        //cortinas arriba
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.0f, 19.250f, -2.99f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(12 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 19.250f, -2.99f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(12 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.0f, 19.250f, -2.99f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(12 * sizeof(int)));

        //cortinas arriba lateral
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(17.61f, 20, -10.28));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(12 * sizeof(int)));
        

        //marcos ventanas
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaNegra));
        //abajo
        //izq
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.75f, 5.0f, 0.02f)); 
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.75f, 7.50f, 0.03f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.25f, 5.0f, 0.03f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        //dentro
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.75f, 5.0f, -0.03f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.75f, 7.50f, -0.03f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.25f, 5.0f, -0.03f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        //marco ventana trasera
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.785f, 4.15, -10.28));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.795f, 4.15, -13.505));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.795, 6.0, -10.28));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        
        //marco ventana lateral izq
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.35f, 5.0f, -25.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.4f, 5.0f, -28.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.4f, 7.0f, -25.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.35f, 5.0f, -40.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.4f, 5.0f, -43.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.4f, 7.0f, -40.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.25f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
 
        //der
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 5.0f, 0.02f));
        model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 7.50f, 0.03f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.25f, 5.0f, 0.03f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        //dentro
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 5.0f, -0.03f));
        model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.75f, 7.50f, -0.03f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.25f, 5.0f, -0.03f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //marco ventanas arriba
        //der
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.0f, 19.250f, -2.98f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.0f, 21.750f, -2.97f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.125f, 19.15f, -2.97f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //centro
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 19.250f, -2.98f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 21.750f, -2.98f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(1.875f, 19.25f, -2.97f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //izq
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.0f, 19.250f, -2.98f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.0f, 21.750f, -2.97f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(9.875f, 19.25f, -2.97f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //marcos ventana lateral arriba
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(17.62f, 20, -10.28));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(17.62f, 22.5, -10.250));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(17.62f, 20, -12.28));
        model = glm::scale(model, glm::vec3(0.75f, 1.0f, 1.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));

        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //Marco puerta
        /*model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.05f, 1.25f, 0.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));*/
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.03f, 6.0f, -1.5f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.375f, 0.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        //dntro
       /* model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-0.02f, 1.25f, -0.0125f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-0.03f, 6.0f, -1.5f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.375f, 0.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));*/

        //marco puerta trasera
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.5f, 0.05f, -60.16f));
        model = glm::scale(model, glm::vec3(1.0f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));

        //marco puerta cochera
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-23.0f, -0.2f, 2.66f));
        model = glm::scale(model, glm::vec3(1.750f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));

        //Lineas Techo
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 15.25f, -1.0f));
        model = glm::rotate(model, glm::radians(-2.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(2.3f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.0f, 14.0f, 0.0f));
        model = glm::scale(model, glm::vec3(4.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(5.0f, 13.0f, 1.0f));
        model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //lineas techo 2 piso
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(8.250f, 24.8, -1.0f));
        model = glm::rotate(model, glm::radians(2.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8, 25.5, -1.6f));
        model = glm::scale(model, glm::vec3(3.5f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-10.0f, 26.25f, -2.35f));
        model = glm::scale(model, glm::vec3(5.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //lineas techo lateral
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.6f, 12.9f, -3.75f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.1f, 14.4f, -4.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(18.1f, 15.4f, -3.75f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
       
        //lineas techo cochera
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-17.0f, 15.16, 3.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(3.5f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-16.0f, 13.75, 1.75f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(3.5f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-13.75f, 10.450, 3.5f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(3.5f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //lineas puerta cochera
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-23.0f, 7.0f, 2.65f));
        model = glm::scale(model, glm::vec3(1.75f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-23.0f, 5.0f, 2.65f));
        model = glm::rotate(model, glm::radians(-1.50f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(1.75f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-23.0f, 3.0f, 2.65f));
        model = glm::scale(model, glm::vec3(1.75f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-23.0f, 1.0f, 2.65f));
        model = glm::scale(model, glm::vec3(1.75f, 1.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        
        //---Escalones
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorEscalon));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 0, 0));
        model = glm::scale(model, glm::vec3(1.0f, 0.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 0, 1.5));
        model = glm::scale(model, glm::vec3(1.0f, 0.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 0, 3));
        model = glm::scale(model, glm::vec3(1.0f, 0.125f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //---
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorEscalon2));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 0, 1.5));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.3f, 0.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(5, 0, 1.5));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.3f, 0.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 0, 3));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.3f, 0.125f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(5, 0, 3));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.3f, 0.125f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 0, 0));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, 0.3f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 1.25, 0));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, 0.3f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 0.625, 1.5));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, 0.3f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //---

        //Techo
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorTecho));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.50f, 16.80f, -3.0f));
        model = glm::rotate(model, glm::radians(135.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(6.0f, 1.50f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //Techo 2 piso
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.50f, 26.80f, -3.0f));
        model = glm::rotate(model, glm::radians(135.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(6.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.50f, 26.90, -23.0f));
        model = glm::rotate(model, glm::radians(225.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(6.0f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-12.50f, 26.8, -23.1f));
        model = glm::scale(model, glm::vec3(6.0f, 1.0f, 4.05f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        

        //techo lateral
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorTechoCochera));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(15.0f, 18.4f, -3.20f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(135.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(3.95f, 2.0f, 0.2));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //techo cochera
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorTechoCochera));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-18.75f, 17.55f, 5.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(4.75f, 2.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-26.0f, 7.45f, 5.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(36.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(4.75f, 2.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //dentro
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorTechoCocheraDentro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-18.745f, 17.50f, 4.80f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(4.7f, 2.45f, 0.95f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-25.98f, 7.43f, 4.80f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(36.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(4.75f, 2.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //chimenea lateral
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorChimeneaLateral));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 6.65f, 0.5f));
        model = glm::scale(model, glm::vec3(0.66f, 2.66f, 1.5f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //piramide
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 15.29f, 0.475f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(7.55, 4, 11));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(75 * sizeof(int)));
        //---
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 23.25f, 0.57));
        model = glm::scale(model, glm::vec3(0.66f, 2.5f, 0.93f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
       

        //chimenea frente
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorChimeneaFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 6.65f, 0.475f));
        model = glm::scale(model, glm::vec3(0.66, 2.66f, 1.5f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 6.65f, 0.5f));
        model = glm::scale(model, glm::vec3(0.66, 2.66, 1.5f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        //piramide
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 15.29f, 0.475f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(7.55, 4, 11));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(87 * sizeof(int)));
        //---
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 23.25f, 0.57));
        model = glm::scale(model, glm::vec3(0.66f, 2.5f, 0.93));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 23.25f, 0.57));
        model = glm::scale(model, glm::vec3(0.66f, 2.5f, 0.93));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));

        //chimenea parte alta
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorChimeneaLateralArriba));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 29.5, 0.57));
        model = glm::scale(model, glm::vec3(0.82f, 0.33f, 1.08));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorChimeneaFrenteArriba));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 29.5, 0.57));
        model = glm::scale(model, glm::vec3(0.82f, 0.33f, 1.08));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 29.5, 0.57));
        model = glm::scale(model, glm::vec3(0.82f, 0.33f, 1.08));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        //quemadores
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorTecho));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 31.0, 1.75));
        model = glm::scale(model, glm::vec3(0.3f, 0.5f, 0.2));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(19.25f, 31.0, -0.75));
        model = glm::scale(model, glm::vec3(0.3f, 0.5f, 0.2));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));

        //ladrillos
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaNegra));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 2.0f, 3.5f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.4f, 0.2f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 3.5f, 3.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.4f, 0.2f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 5.0f, 0.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.4f, 0.3f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 7.75f, 3.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.4f, 0.2f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 9.35f, 1.25f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.4f, 0.15f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 17.75f, 1.25f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.3f, 0.1f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 19.5f, 2.5f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.2f, 0.15f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 21.25f, 1.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.25f, 0.1f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(20.920f, 23.0f, 2.25f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.35f, 0.15f, 1.0));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));

        
        
        //Dentro de la casa
        //--------------------------------

        //techo dentro casa
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorTechoCasa));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.15f, 13.35, -60));
        model = glm::scale(model, glm::vec3(5.15f, 1.0f, 12.0));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //piso
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorSueloHabitacion));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.15f, 1.250, -60));
        model = glm::scale(model, glm::vec3(5.15f, 1.0f, 12.0));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //Pasto
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorPasto));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-75.0f, 0.0f, -75.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(30.0f, 30.0f, 30.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //camino Cochera

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorCamino));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-23.0f, 0.1f, 2.5f));
        model = glm::scale(model, glm::vec3(1.750f, 1.0f, 6.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
       
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        

        //Habitacion
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorParedHabitacion));
        //paredes
        //atras
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.25f, 0.0f, -59.0f));
        model = glm::scale(model, glm::vec3(5.175f, 2.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //frente
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.25f, 0.0f, -24.0f));
        model = glm::scale(model, glm::vec3(3.45f, 2.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.0f, 0.0f, -24.0f));
        model = glm::scale(model, glm::vec3(0.74f, 2.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(6.0f, 10.0f, -24.0f));
        model = glm::scale(model, glm::vec3(1.0f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //laterales
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.2f, 0.0f, -24.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(7.0f, 2.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(14.625f, 0.0f, -24.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(7.0f, 2.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        //linas pared
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaNegra));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.05f, 0.0f, -58.95f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(2.75f, 1.0, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.05f, 0.0f, -24.05f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(2.75f, 1.0, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(14.5f, 0.0f, -58.95f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(2.75f, 1.0, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(14.5f, 0.0f, -24.05f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(2.75f, 1.0, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        //superior
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.18f, 12.75, -58.850));
        model = glm::scale(model, glm::vec3(5.16f, 1.0, 6.95f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        //lineas pared frente 
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.25f, 12.750f, -23.950f));
        model = glm::scale(model, glm::vec3(5.175f, 2.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //lineas suelo habitacion
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineasSueloHabitacion));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-11.25f, 1.3f, -24.0f));
        model = glm::scale(model, glm::vec3(5.175f, 2.75f, 1.0f));
        
        for (size_t i = 0; i < 8; i++) {
            model = glm::translate(model, glm::vec3(0.0f, 0.0f, -4.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        }

        //puerta habitacion
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorPuertaHabitacion));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(6.0f, 1.25f, -24.0f));
        model = glm::rotate(model, glm::radians(rotKitPuerta), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::translate(model, glm::vec3(2.5f, 4.375f, -0.0625f));
        model = glm::scale(model, glm::vec3(1.0f, 1.75f, 0.05f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorPuertaLateralHabitacion));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaNegra));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(5.75f, 1.0f, -23.98f));
        model = glm::scale(model, glm::vec3(1.1f, 1.85f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));

        //--- Elemntos de la habitacion

        //mueble
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.5, 5.5, -55));
        model = glm::scale(model, glm::vec3(1.0f, 0.1f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //---
        //atras
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.5, 4, -57));
        model = glm::scale(model, glm::vec3(0.75f, 0.5f, 0.1f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //debajo
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.5, 3.0, -55.0));
        model = glm::scale(model, glm::vec3(0.6f, 0.1f, 0.75f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //laterales mueble
        //izq
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(9.87, 4, -55));
        model = glm::scale(model, glm::vec3(0.1f, 0.5f, 0.75f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //der
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(13.13, 4, -55));
        model = glm::scale(model, glm::vec3(0.1f, 0.5f, 0.75f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //cajon mueble
        //frente
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, PosIniCajonFrente + glm::vec3(0, 0, movKitZCajon));
        model = glm::scale(model, glm::vec3(0.6f, 0.4f, 0.1f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //base cajon
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.5, 3.5, -54.25) + glm::vec3(0, 0, movKitZCajon));
        model = glm::scale(model, glm::vec3(0.6f, 0.1f, 0.4f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //laterales mueble
        //izq
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(10.25, 4.5, -54.25) + glm::vec3(0, 0, movKitZCajon));
        model = glm::scale(model, glm::vec3(0.1f, 0.3f, 0.4f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //der
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(12.75, 4.5, -54.25) + glm::vec3(0, 0, movKitZCajon));
        model = glm::scale(model, glm::vec3(0.1f, 0.3f, 0.4f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //marcos cajon
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaCajonEscritorio));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(10.750, 3.5, -52.73) + glm::vec3(0, 0, movKitZCajon));
        model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.1f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.25, 3.75, -52.73) + glm::vec3(0, 0, movKitZCajon));
        model = glm::scale(model, glm::vec3(0.1f, 0.05f, 0.1f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        //patas mueble
        //delanteras
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(9.87, 1.5, -53.375));
        model = glm::scale(model, glm::vec3(0.1f, 0.5f, 0.1f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(13.13, 1.5, -53.375));
        model = glm::scale(model, glm::vec3(0.1f, 0.5f, 0.1f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //traseras patas
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(9.88, 1.5, -57.0));
        model = glm::scale(model, glm::vec3(0.1f, 0.5f, 0.1f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(13.13, 1.5, -57.0));
        model = glm::scale(model, glm::vec3(0.1f, 0.5f, 0.1f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMuebleLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));

        //lampara
        //parte superior
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLampara));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.0, 10, -55));
        model = glm::rotate(model, glm::radians(270.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(6.5f, 6.5f, 4.5f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 72, GL_UNSIGNED_INT, (GLvoid*)(99 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineasLampara));
        model = glm::translate(model, glm::vec3(0.0f, 0.01f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINES, 32, GL_UNSIGNED_INT, (GLvoid*)(99 * sizeof(int)));
        model = glm::translate(model, glm::vec3(0.0f, -0.02f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINES, 34, GL_UNSIGNED_INT, (GLvoid*)(137 * sizeof(int)));
        //base lampara
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLampara));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(11.625, 6.5, -55));
        model = glm::scale(model, glm::vec3(2.0f, 2.50f, 5.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, (GLvoid*)(171 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLamparaBase));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(195 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLampara));
        model = glm::translate(model, glm::vec3(0, 0.25, 0));
        model = glm::rotate(model, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, (GLvoid*)(171 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLamparaBase));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(195 * sizeof(int)));

        //librero
        //laterales
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-10.5f, 4.35f, -26.750f));
        model = glm::scale(model, glm::vec3(0.05f, 1.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        //---
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-0.5f, 4.35f, -26.750f));
        model = glm::scale(model, glm::vec3(0.05f, 1.25f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        //Repisas
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-5.5f, 1.35f, -26.750f));
        model = glm::scale(model, glm::vec3(2.0f, 0.05f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-5.5f, 3.43f, -26.750f));
        model = glm::scale(model, glm::vec3(2.0f, 0.05f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-5.5f, 5.51f, -26.750f));
        model = glm::scale(model, glm::vec3(2.0f, 0.05f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-5.5f, 7.35f, -26.750f));
        model = glm::scale(model, glm::vec3(2.0f, 0.05f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        //libros
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.750f, 6.4f, -28.350f));
        model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        //lineas
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.3f, 5.6f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.35f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.7f, 5.6f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.35f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-2.2f, 5.6f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.35f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        //---
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.05f, 6.25f, -28.350f));
        model = glm::scale(model, glm::vec3(0.5f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.2f, 5.645f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.5f, 5.645f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.9f, 5.645f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.5f, 5.645f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.8f, 5.645f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        //---
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.5f, 6.4f, -28.350f));
        model = glm::scale(model, glm::vec3(0.3f, 0.4f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        //lineas
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.55f, 5.6f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.35f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-9.7f, 6.3f, -28.350f));
        model = glm::rotate(model, glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.07f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.7f, 6.4f, -28.350f));
        model = glm::scale(model, glm::vec3(0.07f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        //--- Libros repisa enmedio
        //libros
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-2.3f, 4.4f, -28.350f));
        model = glm::scale(model, glm::vec3(0.6f, 0.4f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.3f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.6f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.9f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-2.2f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-2.6f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-2.9f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.4f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.75f, 4.25f, -28.350f));
        model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.75f, 3.50f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.05f, 4.3f, -28.350f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.07f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.3f, 4.4f, -28.350f));
        model = glm::scale(model, glm::vec3(0.5f, 0.4f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-7.5f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-7.8f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.1f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.4f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.7f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-9.0f, 3.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.4f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

       //libros repisa inferior
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.750f, 2.25f, -28.350f));
        model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
       
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.3f, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.35f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-1.7f, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.35f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-2.1f, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.35f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));

        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-5.05f, 2.15f, -28.350f));
        model = glm::scale(model, glm::vec3(0.8f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.4f, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-3.75f, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.0f, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.45f, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-4.85f, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-5.0, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-5.5, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.0, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.6, 1.5f, -29.115f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.27f, 1.0f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 2, GL_UNSIGNED_INT, (GLvoid*)(10 * sizeof(int)));
        
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-7.6f, 2.2f, -28.350f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.07f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.0f, 2.2f, -28.350f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.07f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.4f, 2.2f, -28.350f));
        model = glm::rotate(model, glm::radians(145.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.07f, 0.3f, 0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));

        //Escritorio base
        //---
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroLateral));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.5f, 3.35f, -55.0f));
        model = glm::scale(model, glm::vec3(0.80f, 1.0f, 0.9f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroFrente));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        //Escritorio superior
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroLateral));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.5f, 5.95f, -51.750f));
        model = glm::scale(model, glm::vec3(1.0f, 0.05f, 2.2f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLibreroFrente));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //cajones escritorio
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaCajonEscritorio));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.47f, 4.250f, -53.50f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.6f, 0.25f, 1.0f));      
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.47f, 1.750f, -53.50f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.6f, 0.4f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(6 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.47f, 4.90f, -55.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.1f, 0.1f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 3, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-6.47f, 3.0f, -55.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, glm::vec3(0.1f, 0.1f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_LINE_LOOP, 3, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //-----
        //computadora
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraLateral));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.5f, 6.265f, -51.750f));
        model = glm::scale(model, glm::vec3(0.6f, 0.08f, 0.6f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraFrente));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraLateral));
        //----
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-9.25f, 6.65f, -51.750f));
        model = glm::scale(model, glm::vec3(0.3f, 0.09f, 0.6f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraFrente));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        //Monitor
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraLateral));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.75f, 7.1f, -51.750f));
        model = glm::scale(model, glm::vec3(0.5f, 0.10f, 0.6f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(24 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(48 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraFrente));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(36 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraLateral));
        //---
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraFrente));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-7.49f, 7.350f, -50.250f));
        model = glm::scale(model, glm::vec3(2.5f, 2.750f, 3.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(207 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, (GLvoid*)(213 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraFrente));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(225 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorPantallaComputadora));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-7.48f, 7.350f, -50.5f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f, 0.5f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorPantallaComputadora));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-8.5f, 7.550f, -50.240f));
        model = glm::scale(model, glm::vec3(0.125f, 0.125f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));

        //Teclado
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-7.50f, 6.0f, -49.50f));
        model = glm::scale(model, glm::vec3(1.0f, 0.4f, 3.0f));
        model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraLateral));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(207 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(219 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorComputadoraFrente));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(225 * sizeof(int)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(213 * sizeof(int)));

        //lampara computadora
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLampara));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(-7.0f, 10.8f, -51.90f));
        model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(-30.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.65f, 1.65f, 1.125f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 72, GL_UNSIGNED_INT, (GLvoid*)(99 * sizeof(int)));

        //cuadro de estrellas
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorMarcoCuadro));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(14.60f, 8.0f, -40.0f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(1.0f, 0.75f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        glUniform3fv(uniformColor, 1, glm::value_ptr(colorLineaNegra));
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(14.55f, 8.50f, -40.50f));
        model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.8f, 0.55f, 1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)(0 * sizeof(int)));
        

       //-----
       // Modelos 

       shaderModel.Use();
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));

       model = glm::mat4(1.0f);
       model = glm::scale(model, glm::vec3(1.25f, 1.25f, 1.25f));
       model = glm::translate(model, glm::vec3(-5, 1.25f, -30.0f));
       model = glm::rotate(model, glm::radians(55.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       timmyModel.Draw(shaderModel);

       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(-5.0f, 6.0f, -15.0f));
       model = glm::scale(model, glm::vec3(0.125f, 0.10f, 0.125f));
       model = glm::rotate(model, glm::radians(50.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       cosmoModel.Draw(shaderModel);

       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(-2.0f, 7.50f, -20.0f));
       model = glm::scale(model, glm::vec3(0.125f, 0.10f, 0.125f));
       model = glm::rotate(model, glm::radians(-10.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       wandaModel.Draw(shaderModel);

       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(0.250f, 8.30f, -24.0f) + glm::vec3(0, movKitYCohete, 0));
       model = glm::rotate(model, glm::radians(-45.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       model = glm::rotate(model, glm::radians(rotKitCohete), glm::vec3(1.0f, 0.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       coheteModel.Draw(shaderModel);
       coheteLateralModel.Draw(shaderModel);
       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(0.0078125f, 8.30f, -24.0f) + glm::vec3(0, movKitYCohete, 0));
       model = glm::rotate(model, glm::radians(-45.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       model = glm::rotate(model, glm::radians(rotKitCohete), glm::vec3(1.0f, 0.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       coheteVentanasModel.Draw(shaderModel);

       //---

       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(13.85f, 9.0f, -44.25));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       estrellaModel.Draw(shaderModel);

       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(13.85f, 9.0f, -42.5));
       model = glm::rotate(model, glm::radians(2.0f), glm::vec3(1.0f, 0.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       estrellaModel.Draw(shaderModel);
      
       //--LamparaComputadora
       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(-7.450f, 10.10f, -51.750f));
       model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       model = glm::rotate(model, glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));
       model = glm::scale(model, glm::vec3(4.50f, 4.50f, 4.50f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       brazoLamparaModel.Draw(shaderModel);
       //foco 
       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(-6.65f, 10.6f, -51.70f));
       model = glm::scale(model, glm::vec3(0.210f, 0.210f, 0.210f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       focoLamparaModel.Draw(shaderModel);
       //Antena

       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(-16.15f, 22.6f, -5.70f));
       model = glm::scale(model, glm::vec3(4.0f, 4.0f, 4.0f));
       model = glm::rotate(model, glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       antenaExtremosModel.Draw(shaderModel);
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       antenaPlatoModel.Draw(shaderModel);

       //Camino puerta

       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(9.250, 0.2, 19.0));
       model = glm::scale(model, glm::vec3(1.0f, 1.0f, 2.0f));
       model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       caminoModel.Draw(shaderModel);

       /*model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(9.250, 5.2, 19.0));
       model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));
       model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       pez1.Draw(shaderModel);

       model = glm::mat4(1.0f);
       model = glm::translate(model, glm::vec3(15.250, 5.2, 19.0));
       model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));
       model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
       glUniformMatrix4fv(glGetUniformLocation(shaderModel.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
       pez2.Draw(shaderModel);*/



        glBindVertexArray( 0 );
        glfwSwapBuffers( window );
    }
    
    glDeleteVertexArrays( 1, &VAO );
    glDeleteBuffers( 1, &VBO );
    glfwTerminate( );
    return EXIT_SUCCESS;

    

}

void DoMovement( ) {

    if (keys[GLFW_KEY_P]) {
        printf("Position (%f, %f, %f) \n", camera.GetPosition().x, camera.GetPosition().y, camera.GetPosition().z);
        printf("Front (%f, %f, %f) \n", camera.GetFront().x, camera.GetFront().y, camera.GetFront().z);
    }

    if (keys[GLFW_KEY_Q]) {
        camera.ProcessKeyboard(Q, deltaTime);
    }

    if (keys[GLFW_KEY_E]) {
        camera.ProcessKeyboard(E, deltaTime);
    }

    if (keys[GLFW_KEY_Z]) {
        camera.ProcessKeyboard(Z, deltaTime);
    }

    if (keys[GLFW_KEY_C]) {
        camera.ProcessKeyboard(C, deltaTime);
    }

    if ( keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
        camera.ProcessKeyboard( FORWARD, deltaTime );
    }
    
    if ( keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
        camera.ProcessKeyboard( BACKWARD, deltaTime );
    }
    
    if ( keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
        camera.ProcessKeyboard( LEFT, deltaTime );
    }
    
    if ( keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
        camera.ProcessKeyboard( RIGHT, deltaTime );
    }

    if (keys[GLFW_KEY_1]) {
        if (!cerrarPuerta && rotKitPuerta < 89) {
            abrirPuerta = true;
        }  
    }
    if (keys[GLFW_KEY_2]) {
        if (!abrirPuerta && rotKitPuerta > 0) {
            cerrarPuerta = true;
        } 
    }

    if (keys[GLFW_KEY_3]) {
        if (!cerrarCajon && movKitZCajon <= 0) {
            abrirCajon = true;
        }
    }
    if (keys[GLFW_KEY_4]) {
        if (!abrirCajon && movKitZCajon >= 2) {
            cerrarCajon = true;
        }
    }
    if (keys[GLFW_KEY_5]) {
        if (!bajarCohete && movKitYCohete <= 2.7) {
            subirCohete = true;
        }
    }
    if (keys[GLFW_KEY_6]) {
        if (!subirCohete && movKitYCohete >= 0 && (rotacionCohete == false && rotacionCohete2 == false)) {
            bajarCohete = true;
        }
    }

}

void animacion() {

    if (abrirPuerta) {
        rotKitPuerta += 0.15;
        if (rotKitPuerta > 89) {
            abrirPuerta = false;
        } 
    }

    if (cerrarPuerta) {
        rotKitPuerta -= 0.15;
        if (rotKitPuerta < 0) {
            cerrarPuerta = false;
        }
    }

    if (abrirCajon) {
        movKitZCajon += 0.01;
        if (movKitZCajon > 2) {
            abrirCajon = false;
        }
    }

    if (cerrarCajon) {
        movKitZCajon -= 0.01;
        if (movKitZCajon < 0) {
            cerrarCajon = false;
        }
    }

    if (subirCohete) {
        movKitYCohete += 0.015;
        if (movKitYCohete > 2.7) {
            subirCohete = false;
            rotacionCohete = true;
        }
    }

    if (rotacionCohete) {
        rotKitCohete += 2;
        if (rotKitCohete > 550) {
            rotacionCohete = false;
            rotacionCohete2 = true;
        }
    }

    if (rotacionCohete2) {
        rotKitCohete -= 2;
        if (rotKitCohete < 0) {
            rotacionCohete2 = false;
        }
    }

    if (bajarCohete) {
        movKitYCohete -= 0.015;
        if (movKitYCohete < 0) {
            bajarCohete = false;
        }
    }
    

}

void KeyCallback( GLFWwindow *window, int key, int scancode, int action, int mode ) {
    if ( GLFW_KEY_ESCAPE == key && GLFW_PRESS == action ) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    
    if ( key >= 0 && key < 1024 ) {
        if ( action == GLFW_PRESS ) {
            keys[key] = true;
        }
        else if ( action == GLFW_RELEASE ) {
            keys[key] = false;
        }
    }
}

void MouseCallback( GLFWwindow *window, double xPos, double yPos ) {
    if ( firstMouse ) {
        lastX = xPos;
        lastY = yPos;
        firstMouse = false;
    }
    
    GLfloat xOffset = xPos - lastX;
    GLfloat yOffset = lastY - yPos;
    
    
    lastX = xPos;
    lastY = yPos;
    
    camera.ProcessMouseMovement( xOffset, yOffset );
}